'use strict';
var Alexa = require('alexa-sdk');
// var http = require('http');

var APP_ID = 'amzn1.ask.skill.2f5b454b-208e-4737-8c24-b0ff937b021b';

//CONSTANTS
var possibleWords = ['potato','truck','branch','wallet','mellon','kitty','program','store','peace','quarter',
						'quick','polite','herd','fancy','fake','stealthy','arrows','phantom','chain','contest',
						'smile','agenda','spell','dinner','humid','cube','thing','hamilton','paper','beehive',
						'cuddly','bumble','jigsaw','fabric','beam','chant','pulse','empire','curse','apricot'];
var randomWord;
var lettersTried = [];
var guessedSoFar;
var MAX_INCORRECT = 6;
var incorrectGuesses = 0;
var gameStarted = false;
var output = '';

exports.handler = function(event, context, callback) {
    var alexa = Alexa.handler(event, context);
    alexa.registerHandlers(handlers);
    alexa.execute();
};

var handlers = {
    'LaunchRequest': function () {
        startGame();
		output = 'Welcome to hangman. Your word has ' + randomWord.length + ' letters';
		this.emit(':ask', output, 'guess a letter or word');
    },
    'CheckLetterIntent': function () {
		var letterGuess = this.event.request.intent.slots.letter.value[0].toLowerCase();
        startGame();
		checkLetter(letterGuess);
		if (checkForLoss()){
			resetGame();
			this.emit(':tell', 'You guessed too many incorrect letters. The word was: ' + randomWord); 
		} else if (checkForWin()) {
			resetGame();
			this.emit(':tell', output, output);
		} else {
			var locations = getLocationsOfLetterInWord(letterGuess)
			if (locations.length === 0) {
				output = 'That letter is not in there';
			} else {
				setLetterPresentMessage(locations);
			}
			this.emit(':ask', output, 'guess another');
		}
    },
    'CheckGuessIntent': function () {
		var wordGuess = this.event.request.intent.slots.wordGuess.value;
		console.log('word - ' + randomWord + ', guess - ' + wordGuess);
		if (typeof randomWord == 'undefined') {
			this.emit(':tell', 'You should start a game first.');
		} else if (wordGuess == randomWord) {
			resetGame();
        	this.emit(':tell', 'Congratulations, you got the word ' + randomWord);
		} else {
			this.emit(':ask', 'keep guessing');
		}
    },
    'SessionEndedRequest': function () {
        this.emit('AMAZON.StopIntent');
    }
};

//stars the game and initializes array to length
function startGame() {
	if (!gameStarted) {
		randomWord = possibleWords[Math.floor(Math.random()*possibleWords.length)]
		guessedSoFar = new Array(randomWord.length);
		gameStarted = true;
	}
}

//check if letter is in word and return locations
function checkLetter(letter) {
	var locations = getLocationsOfLetterInWord(letter);
	if (locations.length === 0) {
		incorrectGuesses++;
	}
	if (lettersTried.indexOf(letter) === -1) {
		lettersTried.push(letter);
	}
}

//get locations in the word where the given letter appears
function getLocationsOfLetterInWord(letter) {
	var locations = [];
	for (var i = 0; i < randomWord.length; i++){
		if (letter == randomWord.charAt(i)){
			locations.push(i+1);
			guessedSoFar[i] = letter;
		}
	}
	return locations;
}

//returns if guess matches the random word
function checkGuessWord(word) {
	if (randomWord === word) {
		return true;
	} else {
		return false;
	}
}

//checks if the player loses
function checkForLoss() {
	if (incorrectGuesses >= MAX_INCORRECT) {
		return true;
	} else {
		return false;
	}
}

//checks if there is victory and sets output
function checkForWin() {
	var guessedAsWord = guessedSoFar.join('');
	if (checkGuessWord(guessedAsWord)) {
        output = 'Congratulations, you got the word ' + randomWord;
		return true;
    } else {
    	return false;
    }
}

//sets letter present message whether one or more
function setLetterPresentMessage(locations) {
    if (locations.length === 1) {
    	output = 'That letter is in location: '; + locations[0] + ' of ' + randomWord.length;
    } else {
    	output = 'That letter is in the following spots: ';
		for (var i of locations) {
			output += i + ', ';
		}
		output += ' of ' + randomWord.length; 
    }
}

//resets values after winning a game
function resetGame() {
	gameStarted = false;
	incorrectGuesses = 0;
}